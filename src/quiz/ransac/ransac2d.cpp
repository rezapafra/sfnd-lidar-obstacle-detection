/* \author Aaron Brown */
// Quiz on implementing simple RANSAC line fitting

#include "../../render/render.h"
#include <unordered_set>
#include "../../processPointClouds.h"
// using templates for processPointClouds so also include .cpp to help linker
#include "../../processPointClouds.cpp"
#include <random>
#include <algorithm>
#include <experimental/algorithm>

pcl::PointCloud<pcl::PointXYZ>::Ptr CreateData()
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
  	// Add inliers
  	float scatter = 0.6;
  	for(int i = -5; i < 5; i++)
  	{
  		double rx = 2*(((double) rand() / (RAND_MAX))-0.5);
  		double ry = 2*(((double) rand() / (RAND_MAX))-0.5);
  		pcl::PointXYZ point;
  		point.x = i+scatter*rx;
  		point.y = i+scatter*ry;
  		point.z = 0;

  		cloud->points.push_back(point);
  	}
  	// Add outliers
  	int numOutliers = 10;
  	while(numOutliers--)
  	{
  		double rx = 2*(((double) rand() / (RAND_MAX))-0.5);
  		double ry = 2*(((double) rand() / (RAND_MAX))-0.5);
  		pcl::PointXYZ point;
  		point.x = 5*rx;
  		point.y = 5*ry;
  		point.z = 0;

  		cloud->points.push_back(point);

  	}
  	cloud->width = cloud->points.size();
  	cloud->height = 1;

  	return cloud;

}

pcl::PointCloud<pcl::PointXYZ>::Ptr CreateData3D()
{
	ProcessPointClouds<pcl::PointXYZ> pointProcessor;
	return pointProcessor.loadPcd("../../../sensors/data/pcd/simpleHighway.pcd");
}


pcl::visualization::PCLVisualizer::Ptr initScene()
{
	pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer ("2D Viewer"));
	viewer->setBackgroundColor (0, 0, 0);
  	viewer->initCameraParameters();
  	viewer->setCameraPosition(0, 0, 15, 0, 1, 0);
  	viewer->addCoordinateSystem (1.0);
  	return viewer;
}

// Sample without replacement over a range using Bob Floyd's algorithm
std::unordered_set<int> sampleWithoutReplacement(int sampleSize, int rangeUpperBound)
{
    std::unordered_set<int> sample;
    std::default_random_engine generator;

    for(int d = rangeUpperBound - sampleSize; d < rangeUpperBound; d++)
    {
        int t = std::uniform_int_distribution<>(0, d)(generator);
        if (sample.find(t) == sample.end() )
            sample.insert(t);
        else
            sample.insert(d);
    }
    return sample;
}

std::unordered_set<int> Ransac(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, int maxIterations, float distanceTol)
{
	std::unordered_set<int> inliersResult;
	srand(time(NULL));

	int sampleSize {2};
	int cloudSize = cloud->width;
	while(maxIterations--) {
	    std::unordered_set<int> inliers;
        std::unordered_set<int> sampleIndices = sampleWithoutReplacement(sampleSize, cloudSize);
        auto iter {sampleIndices.begin()};

        std::vector<pcl::PointXYZ> samplePoints {};
        while (iter != sampleIndices.end())
        {
            samplePoints.push_back(cloud->points.at(*iter));
        }

	    // fit a line ax + by+ c = 0 to the points in sample using (y0−y1)x+(x1−x0)y+(x0∗y1−x1∗y0)=0
	    double x0 {samplePoints.at(0).x};
        double x1 {samplePoints.at(1).x};
        double y0 {samplePoints.at(0).y};
        double y1 {samplePoints.at(1).y};
	    double a {y0 - y1};
	    double b {x1 - x0};
	    double c {x0 * y1 - x1 * y0};

	    // find the distance to this line for all points in the cloud. If the distance is less than
	    // distanceTol at the index of the point to inliers
	    for(int index = 0; index < cloud->points.size(); ++index) {
            pcl::PointXYZ point = cloud->points[index];
	        double distance = fabs(a * point.x - b * point.y - c) / sqrt(std::pow(a, 2) + std::pow(b, 2));
	        if (distance < distanceTol) {
	            // add the index of the point to inliers
	            inliers.insert(index);
	        }
	    }
	    if (inliers.size() > inliersResult.size()) { inliersResult = inliers; }
	}

	return inliersResult;
}

int main ()
{

	// Create viewer
	pcl::visualization::PCLVisualizer::Ptr viewer = initScene();

	// Create data
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = CreateData();
	

	std::unordered_set<int> inliers = Ransac(cloud, 100, 0.1);

	pcl::PointCloud<pcl::PointXYZ>::Ptr  cloudInliers(new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudOutliers(new pcl::PointCloud<pcl::PointXYZ>());

	for(int index = 0; index < cloud->points.size(); index++)
	{
		pcl::PointXYZ point = cloud->points[index];
		if(inliers.count(index))
			cloudInliers->points.push_back(point);
		else
			cloudOutliers->points.push_back(point);
	}


	// Render 2D point cloud with inliers and outliers
	if(inliers.size())
	{
		renderPointCloud(viewer,cloudInliers,"inliers",Color(0,1,0));
  		renderPointCloud(viewer,cloudOutliers,"outliers",Color(1,0,0));
	}
  	else
  	{
  		renderPointCloud(viewer,cloud,"data");
  	}
	
  	while (!viewer->wasStopped ())
  	{
  	  viewer->spinOnce ();
  	}
  	
}
